" Always use system clipboard for cut, copy and paste
set clipboard+=unnamedplus

" Map : to ;
nnoremap ; :

" Sensible configs
let mapleader="\<SPACE>"
set termguicolors
set nowrap
set showcmd
set showmode
set number
set numberwidth=5
set noerrorbells
set hidden
set cursorline

" More natural splits
set splitbelow
set splitright

" Represent tabs with four spaces
set tabstop=4
set shiftwidth=4

" More sensible window navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Save buffer shortcut
noremap <leader>s :update<CR>

" Quit buffer
noremap <leader>q :q<CR>

" Switch between the last two files
nnoremap <leader><leader> <c-^>

" Copy to clipboard
vnoremap  <leader>y  y    " copy the selected text in visual mode
nnoremap  <leader>y  y    " copy the whole line in normal mode
nnoremap  <leader>Y  yg_  " copy the rest of the line

" Paste from clipboard
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

" Quickly edit/reload init.vim file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

" Plugs
call plug#begin()

Plug 'joshdick/onedark.vim'
Plug 'mhartington/oceanic-next'
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'
Plug 'airblade/vim-gitgutter'
Plug 'ctrlpvim/ctrlp.vim'
" Track the engine.
Plug 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'
" Improved language support
Plug 'sheerun/vim-polyglot'
Plug 'mileszs/ack.vim'

call plug#end()

" Theme13
syntax enable
colorscheme onedark

"---------Plugins---------

"/
"/NERDTree
"/
map <C-n> :NERDTreeToggle<CR>

"/
"/Snippets
"/
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

"/
"/CtrlP
"/
let g:ctrlp_custom_ignore = { 'dir': 'vendor$\|node_modules$' }
"/
"/Ack
"/
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
