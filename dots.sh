#!/bin/bash
BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# neovim
ln -s ${BASEDIR}/nvim/init.vim $HOME/.config/nvim/init.vim

# tmux
ln -s ${BASEDIR}/tmux.conf $HOME/.tmux.conf

#fish shell
ln -s ${BASEDIR}/fish/config.fish $HOME/.config/fish/config.fish
ln -s ${BASEDIR}/fish/functions $HOME/.config/fish/functions
